
import {apiKey} from '../component/data/apiKey'
export const Fetch_CONTENT_OF_FORUM_Begin = 'Fetch_CONTENT_OF_FORUM_Begin';
export const Fetch_CONTENT_OF_FORUM_Success = 'Fetch_CONTENT_OF_FORUM_Success';
export const Fetch_CONTENT_OF_FORUM_Error = 'Fetch_CONTENT_OF_FORUM_Error';
export const SET_FORUM_CONTENT = 'SET_FORUM_CONTENT';

export const fetchContentForum = (country, category) => {
    return dispatch => {
        dispatch(fetchContentForumBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category='+category+'&apiKey=' + apiKey)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchContentForumSuccess(data.articles))
            })
            .catch(error => {
                dispatch(fetchContentForumError(error))
            })
    }
}
export const fetchContentForumBegin = () => {
    return {
        type: Fetch_CONTENT_OF_FORUM_Begin
    }
}
export const fetchContentForumSuccess = (contentNews) => {
    return {
        type: Fetch_CONTENT_OF_FORUM_Success,
        contentNews
    }
}
export const fetchContentForumError = (error) => {
    return {
        type: Fetch_CONTENT_OF_FORUM_Error,
        error
    }
}

export const setForumContent = (contentList) => {
    return {
        type: SET_FORUM_CONTENT,
        contentList
    }
}
