import {apiKey} from '../component/data/apiKey'
export const SEARCH_NEWS = 'SEARCH_NEWS'
export const FETCH_NEWS_BEGIN = 'FETCH_NEWS_BEGIN'
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS'
export const FETCH_NEWS_ERROR = 'FETCH_NEWS_ERROR'
export const SET_NEWS_CONTENT = 'SET_NEWS_CONTENT'

export const fetchNewsList = keyword => {
    return dispatch => {
        dispatch(fetchNewsBegin())

        return fetch('https://newsapi.org/v2/everything?q=' + keyword + '&apiKey='+apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchNewsSuccess(data.articles))
            })
            .catch(error => dispatch(fetchNewsError(error))
            )
    }
}


export const searchNews = (value) => {
    return dispatch => {
        dispatch(fetchNewsList(value))
        dispatch(changeText(value))
    }
}
export const changeText = (value) =>{
    return {
        type: SEARCH_NEWS,
        payLoad: value
    }
} 
export const fetchNewsBegin = () => {
    return {
        type: FETCH_NEWS_BEGIN,
    }
}

export const fetchNewsSuccess = news => {
    return {
        type: FETCH_NEWS_SUCCESS,
        payLoad: news
    }
}

export const fetchNewsError = error => {
    return {
        type: FETCH_NEWS_ERROR,
        payLoad: error
    }
}

export const setNewsContent = content => {
    return {
        type : SET_NEWS_CONTENT,
        payLoad: content
    }
}