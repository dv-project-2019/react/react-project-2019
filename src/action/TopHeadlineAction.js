import {apiKey} from '../component/data/apiKey'
export const FETCH_HEADLINE_BEGIN = 'FETCH_HEADLINE_BEGIN'
export const FETCH_HEADLINE_SUCCESS = 'FETCH_HEADLINE_SUCCESS'
export const FETCH_HEADLINE_ERROR = 'FETCH_HEADLINE_ERROR'
export const FETCH_FILTER_DATA_LIST = 'FETCH_FILTER_DATA_LIST'
export const SET_TOP_CONTENT = 'SET_TOP_CONTENT';

export const FetchHeadlineList = filterCountry => {
    return dispatch => {
        dispatch(fetchHeadlineBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + filterCountry + '&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchHeadlineSuccess(data.articles))
            })
            .catch(error => dispatch(fetchHeadlineError(error)))
    }
}

export const fetchFilterDataList = country => {
    return {
        type: FETCH_FILTER_DATA_LIST,
        payLoad: country
    }
}

export const fetchHeadlineBegin = () => {
    return {
        type: FETCH_HEADLINE_BEGIN,
    }
}
export const fetchHeadlineSuccess = lists => {
    return {
        type: FETCH_HEADLINE_SUCCESS,
        payLoad: lists
    }
}
export const fetchHeadlineError = error => {
    return {
        type: FETCH_HEADLINE_ERROR,
        payLoad: error
    }
}
export const setTopContent = (data) => {
    return {
        type: SET_TOP_CONTENT,
        payLoad: data
    }
}
