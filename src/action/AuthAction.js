export const Login = 'LOGIN';
export const Logout = 'LOGOUT';
export const Fetch_Users_Begin = 'FETCH_USERS_BEGIN';
export const Fetch_Users_Success = 'FETCH_USERS_SUCCESS';
export const Fetch_Users_Error = 'FETCH_USERS_ERROR'
export const Change_Username = 'CHANGE_USERNAME';
export const Change_Password = 'CHANGE_PASSWORD';
export const login = () => {
    return {
        type: Login
    }
}
export const logout = () => {
    return {
        type: Logout
    }
}
export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUsersBegin())
        return fetch('http://18.191.121.114:8080/users')
            .then(response => response.json())
            .then(data => {
                dispatch(fetchUsersSuccess(data))
            })
            .catch(error => {
                dispatch(fetchUsersError(error))
            })
    }
}
export const fetchUsersBegin = () => {
    return {
        type: Fetch_Users_Begin
    }
}
export const fetchUsersSuccess = (users) => {
    return {
        type: Fetch_Users_Success,
        users
    }
}
export const fetchUsersError = (error) => {
    return {
        type: Fetch_Users_Error,
        error
    }
}
export const changeUsername = (username) => {
    return {
        type: Change_Username,
        username
    }
}
export const changePassword = (password) => {
    return {
        type: Change_Password,
        password
    }
}