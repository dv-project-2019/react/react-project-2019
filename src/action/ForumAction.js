import {apiKey} from '../component/data/apiKey'
export const SET_COUNTRY_DATA = 'SET_COUNTRY_DATA';
export const SET_SELECTED_VALUE = 'SET_INITIAL_COUNTRY';
export const FETCH_FORUM_OF_BUSINESS_BEGIN = 'FETCH_FORUM_OF_BUSINESS_BEGIN';
export const FETCH_FORUM_OF_BUSINESS_SUCCESS = 'FETCH_FORUM_OF_BUSINESS_SUCCESS';
export const FETCH_FORUM_OF_BUSINESS_ERROR = 'FETCH_FORUM_OF_BUSINESS_ERROR';
export const SET_CATEGORY_DATA = 'SET_CATEGORY_DATA';
export const FETCH_FORUM_OF_ENTERTAIN_BEGIN = 'FETCH_FORUM_OF_ENTERTAIN_BEGIN';
export const FETCH_FORUM_OF_ENTERTAIN_SUCCESS = 'FETCH_FORUM_OF_ENTERTAIN_SUCCESS';
export const FETCH_FORUM_OF_ENTERTAIN_ERROR = 'FETCH_FORUM_OF_ENTERTAIN_ERROR';
export const FETCH_FORUM_OF_GENERAL_BEGIN = 'FETCH_FORUM_OF_GENERAL_BEGIN';
export const FETCH_FORUM_OF_GENERAL_SUCCESS = 'FETCH_FORUM_OF_GENERAL_SUCCESS';
export const FETCH_FORUM_OF_GENERAL_ERROR = 'FETCH_FORUM_OF_GENERAL_ERROR';
export const FETCH_FORUM_OF_HEALTH_BEGIN = 'FETCH_FORUM_OF_HEALTH_BEGIN';
export const FETCH_FORUM_OF_HEALTH_SUCCESS = 'FETCH_FORUM_OF_HEALTH_SUCCESS';
export const FETCH_FORUM_OF_HEALTH_ERROR = 'FETCH_FORUM_OF_HEALTH_ERROR';
export const FETCH_FORUM_OF_SPORTS_BEGIN = 'FETCH_FORUM_OF_SPORTS_BEGIN';
export const FETCH_FORUM_OF_SPORTS_SUCCESS = 'FETCH_FORUM_OF_SPORTS_SUCCESS';
export const FETCH_FORUM_OF_SPORTS_ERROR = 'FETCH_FORUM_OF_SPORTS_ERROR';
export const FETCH_FORUM_OF_SCIENCE_BEGIN = 'FETCH_FORUM_OF_SCIENCE_BEGIN';
export const FETCH_FORUM_OF_SCIENCE_SUCCESS = 'FETCH_FORUM_OF_SCIENCE_SUCCESS';
export const FETCH_FORUM_OF_SCIENCE_ERROR = 'FETCH_FORUM_OF_SCIENCE_ERROR';
export const FETCH_FORUM_OF_TECH_BEGIN = 'FETCH_FORUM_OF_TECH_BEGIN';
export const FETCH_FORUM_OF_TECH_SUCCESS = 'FETCH_FORUM_OF_TECH_SUCCESS';
export const FETCH_FORUM_OF_TECH_ERROR = 'FETCH_FORUM_OF_TECH_ERROR';
export const FETCH_FORUM_TOPIC_BEGIN = 'FETCH_FORUM_TOPIC_BEGIN';
export const FETCH_FORUM_TOPIC_SUCCESS = 'FETCH_FORUM_TOPIC_SUCCESS';
export const FETCH_FORUM_TOPIC_ERROR = 'FETCH_FORUM_TOPIC_ERROR';
export const ON_SEARCH_TOPIC_FORUM = "ON_SEARCH_TOPIC_FORUM";

export const fetchForumDataOfBusinessCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfBusinessBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=business&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfBusinessSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfBusinessError(error)));
    }
}

export const fetchForumDataOfEntertainCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfEntertainBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=entertainment&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfEntertainSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfEntertainError(error)));
    }
}

export const fetchForumDataOfGeneralCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfGeneralBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=general&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfGeneralSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfGeneralError(error)));
    }
}

export const fetchForumDataOfHealthCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfHealthBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=health&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfHealthSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfHealthError(error)));
    }
}

export const fetchForumDataOfSportsCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfSportsBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=health&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfSportsSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfSportsError(error)));
    }
}

export const fetchForumDataOfScienceCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfScienceBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=health&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfScienceSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfScienceError(error)));
    }
}

export const fetchForumDataOfTechnologyCategory = (country) => {
    return dispatch => {
        dispatch(fetchForumDataOfTechnologyBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=health&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumDataOfTechnologySuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumDataOfTechnologyError(error)));
    }

}

export const fetchForumTopics = (country, category) => {
    return dispatch => {
        dispatch(fetchForumTopicBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&category=' + category + '&apiKey=' + apiKey)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchForumTopicSuccess(data.articles))
            })
            .catch(error => dispatch(fetchForumTopicError(error)));
    }
}

export const onSearchTopic = (data) => {
    return {
        type: ON_SEARCH_TOPIC_FORUM,
        data
    }
}

export const setCountryData = (data) => {
    return {
        type: SET_COUNTRY_DATA,
        data
    }
}

export const setSelectedValue = (countryValue) => {
    return {
        type: SET_SELECTED_VALUE,
        countryValue
    }
}

export const setCategotyData = (data) => {
    return {
        type: SET_CATEGORY_DATA,
        data
    }
}

export const fetchForumDataOfBusinessBegin = () => {
    return {
        type: FETCH_FORUM_OF_BUSINESS_BEGIN
    }
}

export const fetchForumDataOfBusinessSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_BUSINESS_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfBusinessError = (error) => {
    return {
        type: FETCH_FORUM_OF_BUSINESS_ERROR,
        error
    }
}
export const fetchForumTopicBegin = () => {
    return {
        type: FETCH_FORUM_TOPIC_BEGIN
    }
}
export const fetchForumTopicSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_TOPIC_SUCCESS,
        forumData
    }
}

export const fetchForumTopicError = (error) => {
    return {
        type: FETCH_FORUM_TOPIC_ERROR,
        error
    }
}


export const fetchForumDataOfEntertainBegin = () => {
    return {
        type: FETCH_FORUM_OF_ENTERTAIN_BEGIN
    }
}

export const fetchForumDataOfEntertainSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_ENTERTAIN_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfEntertainError = (error) => {
    return {
        type: FETCH_FORUM_OF_ENTERTAIN_ERROR,
        error
    }
}
export const fetchForumDataOfGeneralBegin = () => {
    return {
        type: FETCH_FORUM_OF_GENERAL_BEGIN
    }
}

export const fetchForumDataOfGeneralSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_GENERAL_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfGeneralError = (error) => {
    return {
        type: FETCH_FORUM_OF_GENERAL_ERROR,
        error
    }
}
export const fetchForumDataOfHealthBegin = () => {
    return {
        type: FETCH_FORUM_OF_HEALTH_BEGIN
    }
}

export const fetchForumDataOfHealthSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_HEALTH_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfHealthError = (error) => {
    return {
        type: FETCH_FORUM_OF_HEALTH_ERROR,
        error
    }
}

export const fetchForumDataOfSportsBegin = () => {
    return {
        type: FETCH_FORUM_OF_SPORTS_BEGIN
    }
}

export const fetchForumDataOfSportsSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_SPORTS_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfSportsError = (error) => {
    return {
        type: FETCH_FORUM_OF_SPORTS_ERROR,
        error
    }
}
export const fetchForumDataOfScienceBegin = () => {
    return {
        type: FETCH_FORUM_OF_SCIENCE_BEGIN
    }
}

export const fetchForumDataOfScienceSuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_SCIENCE_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfScienceError = (error) => {
    return {
        type: FETCH_FORUM_OF_SCIENCE_ERROR,
        error
    }
}

export const fetchForumDataOfTechnologyBegin = () => {
    return {
        type: FETCH_FORUM_OF_TECH_BEGIN
    }
}

export const fetchForumDataOfTechnologySuccess = (forumData) => {
    return {
        type: FETCH_FORUM_OF_TECH_SUCCESS,
        forumData
    }
}

export const fetchForumDataOfTechnologyError = (error) => {
    return {
        type: FETCH_FORUM_OF_TECH_ERROR,
        error
    }
}
