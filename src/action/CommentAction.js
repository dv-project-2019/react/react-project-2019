export const FETCH_COMMENT_DATA_BEGIN = 'FETCH_COMMENT_DATA_BEGIN';
export const FETCH_COMMENT_DATA_SUCCESS = 'FETCH_COMMENT_DATA_SUCCESS';
export const FETCH_COMMENT_DATA_ERROR = ' FETCH_COMMENT_DATA_ERROR';
export const ADD_COMMENT = 'ADD_COMMENT';
export const SET_NEW_COMMENT = 'SET_NEW_COMMENT';

export const fetchComment = () => {
    return dispatch => {
        dispatch(fetchCommentBegin())
        return fetch('http://5e44e373e85a4e001492c342.mockapi.io/comment')
            .then(response => response.json())
            .then(data => {
                dispatch(fetchCommentSuccess(data))
            })
            .catch(error => {
                dispatch(fetchCommentError(error))
            })
    }
}
export const fetchCommentBegin = () => {
    return {
        type: FETCH_COMMENT_DATA_BEGIN
    }
}
export const fetchCommentSuccess = (commentList) => {
    return {
        type: FETCH_COMMENT_DATA_SUCCESS,
        commentList
    }
}
export const fetchCommentError = (error) => {
    return {
        type: FETCH_COMMENT_DATA_ERROR,
        error
    }
}
export const addComment = (comment) => {
    return {
        type: ADD_COMMENT, comment
    }
}
export const setNewComment = (commentList) => {
    return {
        type: SET_NEW_COMMENT,
        commentList
    }
}