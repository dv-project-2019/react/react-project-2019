export const SET_FOLLOW_US_DATA = 'SET_FOLLOW_US_DATA';

export const setFollowUsData = (data) => {
    return {
        type: SET_FOLLOW_US_DATA,
        data
    }

}