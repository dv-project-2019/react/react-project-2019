import {apiKey} from '../component/data/apiKey'
export const Fetch_Top_News = 'FETCH_TOP_NEWS';
export const Fetch_Top_News_Begin = 'FETCH_TOP_NEWS_BEGIN';
export const Fetch_Top_News_Success = 'FETCH_TOP_NEWS_SUCCESS';
export const Fetch_Top_News_Error = 'FETCH_TOP_NEWS_ERROR';
export const Fetch_News_Begin = 'FETCH_NEWS_BEGIN';
export const Fetch_News_Success = 'FETCH_NEWS_SUCCESS';
export const Fetch_News_Error = 'FETCH_NEWS_ERROR';
export const Search = 'SEARCH';
export const SetContent = 'SET_CONTENT';
export const Change_Country_Text = 'CHANGE_COUNTRY_TEXT';

export const fetchTopNews = (country) => {
    return dispatch => {
        dispatch(fetchTopNewsBegin())
        return fetch('https://newsapi.org/v2/top-headlines?country=' + country + '&pageSize=6&apiKey=' + apiKey)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchTopNewsSuccess(data.articles))
            })
            .catch(error => {
                dispatch(fetchTopNewsError(error))
            })
    }
}
export const changeCountry = (value) => {
    return dispatch => {
        dispatch(fetchTopNews(value))
        dispatch(changeCountryText(value))
    }
}
export const changeCountryText = (value) => {
    return {
        type: Change_Country_Text,
        value
    }
}
export const fetchTopNewsBegin = () => {
    return {
        type: Fetch_Top_News_Begin
    }
}
export const fetchTopNewsSuccess = (topnews) => {
    return {
        type: Fetch_Top_News_Success,
        topnews
    }
}
export const fetchTopNewsError = (error) => {
    return {
        type: Fetch_Top_News_Error,
        error
    }
}

export const fetchNews = (text) => {
    return dispatch => {
        dispatch(fetchNewsBegin())
        return fetch('https://newsapi.org/v2/everything?q=' + text + '&pageSize=6&apiKey=' + apiKey)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchNewsSuccess(data.articles))
            })
            .catch(error => {
                dispatch(fetchNewsError(error))
            })
    }
}

export const fetchNewsBegin = () => {
    return {
        type: Fetch_News_Begin
    }
}
export const fetchNewsSuccess = (news) => {
    return {
        type: Fetch_News_Success,
        news
    }
}
export const fetchNewsError = (error) => {
    return {
        type: Fetch_News_Error,
        error
    }
}

export const search = (value) => {

    return dispatch => {
        dispatch(fetchNews(value))
        dispatch(changeSerchText(value))

    }
}
export const changeSerchText = (value) => {
    return {
        type: Search,
        value
    }
}
export const setContent = (content) => {
    return {
        type: SetContent,
        content
    }
}
