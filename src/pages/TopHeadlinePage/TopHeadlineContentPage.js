import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import TopHeadlineTopic from '../../component/topheadline-content/topHeadline-topic';
import TopHeadlineContent from '../../component/topheadline-content/topHeadline-content';
import './topheadlineContentPage-style.css';
import ShareContent from '../../component/topheadline-content/shareContent';
import FooterTab from '../../component/FooterTab'
import { FetchHeadlineList, setTopContent } from '../../action/TopHeadlineAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const TopHeadlineContentPage = (props) => {
    const { FetchHeadlineList, data, setTopContent, content } = props;

    useEffect(() => {
        FetchHeadlineList(props.match.params.country);
    }, [])

    setTopContent(data[props.match.params.id])

    return (
        <div className="TopHeadlineContentPage">
            <Menubar />
            {content !== undefined ?
                <div className=" container">
                    <div className="TopHeadlineContent">
                        <TopHeadlineTopic title={content.title} author={content.author} publishedAt={content.publishedAt} />
                        <div className='bottomList'><TopHeadlineContent title={content.title} urlToImage={content.urlToImage} description={content.description} url={content.url} /></div>
                        <hr className="underline-content" />
                        <ShareContent url={content.url} title={content.title} />
                    </div>
                </div>
                :
                <div></div>
            }
            <FooterTab/>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        data: state.TopHeadlineReducer.data,
        content: state.TopHeadlineReducer.content,
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ FetchHeadlineList, setTopContent }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(TopHeadlineContentPage);