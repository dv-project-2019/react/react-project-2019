import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import TopHeadlineList from '../../component/TopHeadline/TopHeadlineList';
import TopHeadlineFilter from '../../component/TopHeadline/TopHeadLineFilter';
import { connect } from 'react-redux';
import { FetchHeadlineList } from '../../action/TopHeadlineAction';
import { bindActionCreators } from 'redux';
import './topHeadLine.css'
import FooterTab from '../../component/FooterTab';

const TopHeadlinePage = (props) => {

    const { FetchHeadlineList } = props

    useEffect(() => {
        FetchHeadlineList(props.match.params.country)

    }, [props.match.params.country])
    console.log(props.match.params.country)
    return (
        <div className="TopHeadlinePage">
            <Menubar />
            <div class="container">
                <div className='whiteBg'>
                    <div className="line-topHeadline">Top Headlines</div>
                    <hr className="underline-topHeader" />

                    <div className="TopHeadlineFilter">
                        < TopHeadlineFilter country={props.match.params.country} />
                    </div>
                    <div className="TopHeadlineList">
                        <TopHeadlineList country={props.match.params.country} />
                    </div>
                </div>
            </div>
            <FooterTab/>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        filter: state.TopHeadlineReducer.filter
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ FetchHeadlineList }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TopHeadlinePage);
