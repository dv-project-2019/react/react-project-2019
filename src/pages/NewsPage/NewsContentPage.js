import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import './News.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NewsTopic from '../../component/News-content/News-topic';
import NewsContent from '../../component/News-content/News-content';
import NewsShareContent from '../../component/News-content/NewsShareContent';
import { fetchNewsList, setNewsContent } from '../../action/NewsPageAction';
import FooterTab from '../../component/FooterTab';

const NewsContentPage = (props) => {
    const { fetchNewsList, data, setNewsContent, content } = props;

    useEffect(() => {
        fetchNewsList(props.match.params.keyword);
    }, [])

    setNewsContent(data[props.match.params.id])

    return (
        <div className="TopHeadlineContentPage">
            <Menubar />
            {content !== undefined ?
                <div className="container">
                    <div className="TopHeadlineContent">
                        <NewsTopic title={content.title} author={content.author} publishedAt={content.publishedAt} />
                        <div className='bottomList'>
                            <NewsContent title={content.title} urlToImage={content.urlToImage} description={content.description} url={content.url} />
                        </div>
                        <hr className="underline-content" />
                        <NewsShareContent url={content.url} title={content.title} />
                    </div>

                </div>
                :
                <div></div>
            }
            <FooterTab/>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        data: state.NewsPageReducer.data,
        content: state.NewsPageReducer.content,
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchNewsList, setNewsContent }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(NewsContentPage);