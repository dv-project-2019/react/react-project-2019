import React, { useEffect } from 'react'
import NewsList from '../../component/NewsPage/NewsList'
import NewsSearch from '../../component/NewsPage/NewsSearch'
import Menubar from '../../component/Header'
import { bindActionCreators } from 'redux'
import { Tag } from 'antd';
import { connect } from 'react-redux'
import { fetchNewsList, searchNews } from '../../action/NewsPageAction';
import './News.css';
import FooterTab from '../../component/FooterTab'


const NewsPage = (props) => {

    const { fetchNewsList, searchNews } = props

    useEffect(() => {
        fetchNewsList(props.match.params.keyword)
    }, [props.match.params.keyword])

    return (
        <div className='TopHeadlineContentPage'>
            <Menubar />
            <div className="container">
                <div className="newsContent">
                    <div className='mid-text'>
                        <p className='fontSize' >News</p>
                        <hr className='bottomLine' />
                    </div>

                    <div className="newsListPart">
                        <div className='row d-flex justify-content-end '>
                            <Tag color="red" style={{ paddingTop: 5, fontSize: 14 }}>
                                Keyword: <b>{props.match.params.keyword}</b>
                            </Tag>
                            <NewsSearch search={(value) => searchNews(value)} />
                        </div>
                        <div className="list">
                            <hr />
                            <NewsList keyword={props.match.params.keyword} />
                        </div>

                    </div>
                </div>
            </div>
            <FooterTab/>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        searchText: state.NewsPageReducer.searchText
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchNewsList, searchNews }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);

