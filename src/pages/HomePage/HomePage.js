import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import CarouselZone from '../../component/HomePage/Carousel';
import TopHeadlineZone from '../../component/HomePage/TopHeadlineZone';
import { fetchTopNews, fetchNews, changeCountry, search } from '../../action/HomeAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FooterTab from '../../component/FooterTab'

import { NewsZone } from '../../component/HomePage/NewsZone';

const HomePage = (props) => {
    const { fetchTopNews, fetchNews, changeCountry, search, topnews, news, searchText, country, isLoading } = props;
    useEffect(() => {
        fetchTopNews(country);
        fetchNews(searchText)
    }, [])
    console.log(isLoading)

    return (
        <div className="main-homepage">
            <Menubar />
            <CarouselZone />
            <br />
            <div style={{ padding: '0 150px 30px 150px' }}>
                <TopHeadlineZone head='Top Headlines' country={country} data={topnews.slice(0, 3)} change={(value) => changeCountry(value)} loading={isLoading} />
                <br />
                <br />
                <NewsZone head='News' data={news} searchText={searchText} search={(value) => search(value)} loading={isLoading} />
            </div>
            <FooterTab/>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        topnews: state.home.topnews,
        news: state.home.news,
        searchText: state.home.searchText,
        country: state.home.country,
        isLoading: state.home.loading
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchTopNews, fetchNews, changeCountry, search }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);