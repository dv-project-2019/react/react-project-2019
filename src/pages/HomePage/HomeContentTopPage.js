import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import { fetchTopNews, setContent } from '../../action/HomeAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './home.css'
import ContentTopic from '../../component/HomePage/content/ContentTopics';
import ShareContent from '../../component/topheadline-content/shareContent';
import ContentDetail from '../../component/HomePage/content/ContentDetail';
import FooterTab from '../../component/FooterTab';

const HomeContentTopPage = (props) => {
    const { fetchTopNews, setContent, topnews, content } = props;
    useEffect(() => {
        fetchTopNews(props.match.params.country);
    }, [])
    setContent(topnews[props.match.params.index]);

    return (
        <div className="TopHeadlineContentPage">
            <Menubar />
            <div style={{ padding: '0 300px' }}>
                {content !== undefined ?
                    <div className="container">
                        <div className="TopHeadlineContent">
                            <ContentTopic title={content.title} author={content.author} publishedAt={content.publishedAt} />
                            <ContentDetail title={content.title} urlToImage={content.urlToImage} description={content.description} url={content.url} />
                            <hr className="underline-content" />
                            <ShareContent url={content.url} title={content.title} />
                        </div>
                    </div>
                    :
                    <div></div>
                }
            </div>
            <FooterTab/>
        </div>

    )
}
const mapStateToProps = state => {
    return {
        topnews: state.home.topnews,
        content: state.home.content,
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchTopNews, setContent }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeContentTopPage);
