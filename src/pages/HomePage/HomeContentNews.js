import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import { fetchNews, setContent } from '../../action/HomeAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './home.css'
import ContentTopic from '../../component/HomePage/content/ContentTopics';
import ContentDetail from '../../component/HomePage/content/ContentDetail';
import ShareContent from '../../component/topheadline-content/shareContent';

const HomeContentNews = (props) => {

    const { setContent, fetchNews, content, news } = props;

    useEffect(() => {
        fetchNews(props.match.params.searchText);
    }, [])

    setContent(news[props.match.params.index]);

    return (
        <div className="TopHeadlineContentPage">
            <Menubar />
            <div style={{ padding: '0 300px' }}>
                {content !== undefined ?
                    <div className="container">
                        <div className="TopHeadlineContent">
                            <ContentTopic title={content.title} author={content.author} publishedAt={content.publishedAt} />
                            <ContentDetail title={content.title} urlToImage={content.urlToImage} description={content.description} url={content.url} />
                            <hr className="underline-content" />
                            <ShareContent url={content.url} title={content.title} />
                        </div>
                    </div>
                    :
                    <div></div>
                }
            </div>
        </div>

    )
}
const mapStateToProps = state => {
    return {
        news: state.home.news,
        content: state.home.content,
        searchText: state.home.searchText
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchNews, setContent }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeContentNews);