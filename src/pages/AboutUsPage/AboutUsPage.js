import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import { Layout, Row, Col, Card } from 'antd';
import './aboutUs.css';
import { memberData } from '../../component/data/memberData';
import { bindActionCreators } from 'redux';
import { setFollowUsData } from '../../action/aboutUsAction';
import { connect } from 'react-redux';
import { Icon } from 'antd';
import { Tooltip } from 'antd';

const AboutUsPage = (props) => {
    const { Content, Footer } = Layout;
    const { Meta } = Card;
    const { setFollowUsData, memberList } = props;

    useEffect(() => {
        setFollowUsData(memberData);
    })

    return (
        <div>
            <Layout>
                <Menubar />
                <Content className="contentPage">
                    <div className="areaPageAb">
                        <b className="sizeAboutUs">
                            ABOUT US
                        </b>
                        <hr className="line-aboutUs" />
                    </div>

                    <div className="bg-ourStory">
                        <Row type="flex" justify="space-around" align="middle">
                            <Col span={10}>
                                <div className="position-TL-ourStory">
                                    <b className="ourStory-text">
                                        OUR STORY
                                    </b>
                                </div>
                            </Col>
                            <Col span={12}>
                                <div className="descript-OurStory">
                                    <p className="color-descrip-our">
                                        This is the news website and forum that we created to show the news all around the world that you can searched by keyword that you interest
                                        or check the news by category that you interested.
                                </p>
                                </div>

                            </Col>
                        </Row>
                    </div>

                    <div className="area-Follow">
                        <div className="follow-text-block">
                            <p className="followUs-text"><b>FOLLOW US</b></p>
                            <hr className="line-aboutUs" />
                        </div>

                        <Row gutter={5} type="flex" justify="space-between" align="middle">
                            {memberList.map((member) => {
                                return (
                                    <Col className="gutter-row" span={4}>
                                        <Card
                                            style={{
                                                width: 240
                                            }}
                                            cover={
                                                <img
                                                    alt={member.firstName}
                                                    src={member.imgUrl}
                                                />
                                            }
                                            actions={[
                                                <Tooltip placement="top" title={member.email} >
                                                    <Icon type="mail" key="mail"/>
                                                </Tooltip>
                                            ]}   
                                        >
                                            <Meta
                                                title={member.firstName + " " + member.lastName}
                                                description={
                                                    <b>{"Student ID: " + member.studentId}</b>
                                                }
                                            />
                                        </Card>
                                    </Col>
                                )
                            })
                            }
                        </Row>
                    </div>
                </Content>

                <Footer style={{ background: '#474747'}}>
                    <Row >
                        <Col span={12}>
                            <b style={{ color: 'white', fontSize: '32px' }}>ADRESS</b>
                            <p style={{ color: 'white', fontSize: '18px', fontWeight: 250 }}>Styal Rd, Styal, Wilmslow SK9 4LA England</p>
                        </Col>
                        <Col span={12} style={{ textAlign: 'end' }}>
                            <b style={{ color: 'white', fontSize: '32px' }}>OUR.COM</b>
                            <p style={{ color: 'white', fontSize: '18px', fontWeight: 250 }}>Copy right © 2020</p>
                        </Col>
                    </Row>
                </Footer>
            </Layout>
        </div>
    )
}

const mapStateTOProps = (state) => {
    return {
        memberList: state.aboutUsData.memberList
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ setFollowUsData }, dispatch);
}
export default connect(mapStateTOProps, mapDispatchToProps)(AboutUsPage);