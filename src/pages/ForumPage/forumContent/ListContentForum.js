import React, { useEffect } from 'react';
import { fetchContentForum, setForumContent } from '../../../action/ContentForumAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Layout, Button, Icon } from 'antd';
const { Content } = Layout;
const ButtonGroup = Button.Group;
const ListContentForum = (props) => {

    const { fetchContentForum, setForumContent, contentList, contentNews } = props;
    useEffect(() => {
        fetchContentForum(props.country, props.category);

    }, [])

    setForumContent(contentNews[props.id]);

    return (
        <div>
            <Layout className="layout">
                <Content className='paddingPageNews'>
                    {contentList !== undefined ?
                        <div className='bgNews '>
                            <div className='positionTypeNews'>
                                <h5 className='colorDateOrWriter'>{props.category.toUpperCase()}</h5>
                                <h1>{contentList.title}</h1>
                                <div class='row'>
                                    <div className='leftDateOrwriter'><h6 className='colorDateOrWriter'>{contentList.publishedAt}</h6></div>
                                    <div className='leftDateOrwriter'><h6 className='colorDateOrWriter'>{contentList.author}</h6></div>
                                </div>
                                <div class="d-flex justify-content-center" className='topImgNew'><img className='sizeImg' src={contentList.urlToImage} /></div>
                                <div class='row' className='topDescrip'>
                                    <div class="col-lg-9 mx-auto" className='descripFont'>
                                        <p>{contentList.description}Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                    </div>
                                    <div class="col-lg-9 mx-auto" className='leftRef'>
                                        <p>Reference:{contentList.url} </p>
                                    </div>
                                    <div class="col-lg-9 mx-auto" className='grayFont'>
                                        <div class='row lf'>
                                            <ButtonGroup>
                                                <Button className='sizeButtonInForum' type="danger">
                                                    <Icon type="reddit" />
                                                    <a href={"https://www.reddit.com/sharer.php?u=" + contentList.url} target="_blank" className='white'>
                                                        reddit
                                                    </a>
                                                </Button>
                                                <Button className='sizeface' type="danger">
                                                    <a href={"https://www.facebook.com/sharer.php?u=" + contentList.url} target="_blank" className='white'>
                                                        <Icon type="facebook" />
                                                        facebook
                                                    </a>
                                                </Button>
                                                <Button className='sizeface' type="danger">
                                                    <Icon type="twitter" />
                                                    <a href={"https://twitter.com/share?url=" + contentList.url + "&amp;text=" + contentList.title} target="_blank" className='white'>
                                                        twitter
                                                    </a>
                                                </Button>
                                            </ButtonGroup>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        :
                        <div></div>
                    }
                </Content>
            </Layout>
        </div>
    )
}
const mapStateToProps = state => {
    return {
        contentNews: state.forumContentData.contentNews,
        contentList: state.forumContentData.contentList,
        selectedValue: state.forumContentData.selectedValue
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchContentForum, setForumContent }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ListContentForum);
