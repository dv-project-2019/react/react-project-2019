import React, { useState } from 'react';
import ImgComment from '../../../assets/iconComment.png';
import { fetchComment, addComment, setNewComment } from '../../../action/CommentAction';
import { bindActionCreators } from 'redux';
import { connect, useDispatch } from 'react-redux';
import { Button, Icon, Layout, BackTop } from 'antd';
import './CommentStyle.css';
const { Content } = Layout;
const Comment = (props) => {

    const [newComment, setNewComment] = useState('')
    const dispatch = useDispatch('')
    const { userdata,comments } = props;

    const clickAdd = () => {
        if (userdata === null) {
            let newsComment = {
                pageId: props.pageId,
                username: 'Guest',
                descriptionComment: newComment
            }
            dispatch(addComment(newsComment))
            setNewComment('');
        } else {
            let newsComment = {
                pageId: props.pageId,
                username: userdata.username,
                descriptionComment: newComment
            }
            dispatch(addComment(newsComment))
            setNewComment('');
        }
    }
    return (
        <div>
            <Layout classNameName="layout">
                <Content style={{ padding: '0 50px',}} className="contentPart">
                    <div style={commentStyle.pageAllComment}>
                        <div className='col-sm'>
                            <h1 style={commentStyle.BorderPageComment}><b style={commentStyle.rdd}>Comment</b></h1>
                        </div>
                        <div className='col-sm '>
                            <form>
                                <div className='row ' style={commentStyle.pageNews}>
                                    <div className="col-1">
                                        <img src={ImgComment} style={commentStyle.imgNews} />
                                    </div>
                                    <div className='col-9' id='forumComment'>
                                        {userdata === null ?
                                            <div className="form-group">
                                                <input type="text" className="form-control" style={commentStyle.shortAreaInput} id="commentName" placeholder="Input name" defaultValue="" value="Guest" disabled />
                                            </div>
                                            :

                                            <div className="form-group">
                                                <input type="text" className="form-control" style={commentStyle.shortAreaInput} id="commentName" placeholder="Input name" defaultValue="" value={userdata.username} disabled />
                                            </div>
                                        }

                                        <div className="form-group">
                                            <textarea className="form-control" id="textArea" style={commentStyle.shortAreaInput} rows="5" placeholder="comment" defaultValue="" value={newComment} onChange={(event) => setNewComment(event.target.value)} />

                                        </div>
                                    </div>
                                    <div className='col-2'>
                                        <div className="form-group d-flex justify-content-end">
                                            <Button style={commentStyle.buttonSubmit} type="danger" onClick={clickAdd}><Icon />Submit</Button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form>
                                {
                                    comments.filter(m => m.pageId === props.pageId).map((data) => {
                                        console.log(data.username);
                                        return (
                                            <div className='row' style={commentStyle.rowPageComment}>
                                                <div className="col-1">
                                                    <img src={ImgComment} style={commentStyle.iconComment} />
                                                </div>
                                                <div className='col-9'>
                                                    <div className='row' style={commentStyle.styleWriter}>
                                                        <div className="form-group">
                                                            <h5 style={commentStyle.stylePostBy}>{data.username}</h5>
                                                        </div>
                                                    </div>
                                                    <div className="form-group" style={commentStyle.divSendComment}>
                                                        <p style={commentStyle.fontComment}>{data.descriptionComment}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })}

                            </form>
                        </div>
                    </div>
                    <div >
                        <BackTop style={commentStyle.lerftMr}>
                            <div style={commentStyle.styleTopmenu}><h4 style={commentStyle.white}>Up</h4></div>
                        </BackTop>
                    </div>
                </Content>
            </Layout>
        </div >
    )
}


const commentStyle =
{
    BorderPageComment: {
        width: '90%',
        lineHeight: '0.1em',
        borderBottom: '3px solid white',
        marginTop: '10px',
        color: 'white'
    },
    white: { color: 'white' },
    rdd: { backgroundColor: '#BE5353' },
    styleTopmenu: {
        backgroundColor: '#ff2454', padding: '30px', width: '90px', borderRadius: '100%'
    },
    lerftMr: {
        marginBottom: '100px'
    },
    pageNews: {
        marginTop: '30px',
        backgroundColor: '#f9d9d9',
        paddingTop: '15px',
        paddingBottom: '10px',
        marginLeft: '15px',
        width: '850px'
    },
    imgNews: {
        width: '160px',
        height: '160px',
        marginTop: '20px'
    },
    buttonSubmit: {
        width: '140px',
        height: '70px',
        marginTop: '120px',
        marginRight: '20px',
        borderRadius: '10px'
    },
    rowPageComment: {
        marginTop: '20px',
        backgroundColor: 'rgba(255, 203, 203, 0.51)',
        paddingTop: '10px',
        paddingLeft: '10px',
        paddingBottom: '10px',
        marginLeft: '95px',
        width: '700px'
    },
    iconComment: {
        width: '130px',
        height: '130px',
        marginTop: '30px'
    },
    styleWriter: {
        marginLeft: '20px'
    },
    stylePostBy: {
        marginLeft: '60px'
    },
    divSendComment: {
        backgroundColor: 'white',
        marginLeft: '80px',
        paddingTop: '40px',
        paddingLeft: '40px',
        paddingRight: '40px',
        paddingBottom: '40px'
    },
    fontComment: {
        fontSize: '20px',
        fontWeight: '300'
    },
    pageAllComment: {
        backgroundColor: '#BE5353',
        padding: '30px 50px 30px 50px',
        minHeight: 280
    },
    shortAreaInput: {
        width: '400px',
        marginLeft: '100px'
    }
}

const mapStateToProps = state => {
    return {
        comments: state.dataComment.comments,
        userdata: state.auth.userdata
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchComment, addComment, setNewComment }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Comment);
