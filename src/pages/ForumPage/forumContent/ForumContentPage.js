import React from 'react';
import Menubar from '../../../component/Header';
import ListContentForum from './ListContentForum';
import Comment from './Comment';
import FooterTab from '../../../component/FooterTab'
import './CommentStyle.css';

const ForumContentPage = (props) => {
    return (
        <div className='bgGray'>
            <Menubar />
            <div class='container'>
                <ListContentForum id={props.match.params.id} country={props.match.params.country} category={props.match.params.category} />
                <Comment pageId={props.match.params.id} country={props.match.params.country} category={props.match.params.category} />
            </div>
                <FooterTab>create by 2019</FooterTab>
        </div>
    )
}
export default ForumContentPage;