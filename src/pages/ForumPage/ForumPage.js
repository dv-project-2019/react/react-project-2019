import React, { useEffect } from 'react';
import Menubar from '../../component/Header';
import './forum-style.css';
import SelectCountry from '../../component/forumPage/selectCountry';
import ForumList from '../../component/forumPage/forumList';
import { Row, Col } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FooterTab from '../../component/FooterTab'
import { countryData } from '../../component/data/countryData';
import {
    setCountryData,
    fetchForumDataOfBusinessCategory,
    fetchForumDataOfEntertainCategory,
    fetchForumDataOfGeneralCategory,
    fetchForumDataOfHealthCategory,
    fetchForumDataOfSportsCategory,
    fetchForumDataOfScienceCategory,
    fetchForumDataOfTechnologyCategory
} from '../../action/ForumAction';

const ForumPage = (props) => {
    const {
        setCountryData,
        countryId,
        fetchForumDataOfBusinessCategory,
        fetchForumDataOfEntertainCategory,
        fetchForumDataOfGeneralCategory,
        fetchForumDataOfHealthCategory,
        fetchForumDataOfSportsCategory,
        fetchForumDataOfScienceCategory,
        fetchForumDataOfTechnologyCategory } = props;

    useEffect(() => {
        setCountryData(countryData);
        fetchForumDataOfBusinessCategory(countryId);
        fetchForumDataOfEntertainCategory(countryId);
        fetchForumDataOfGeneralCategory(countryId);
        fetchForumDataOfHealthCategory(countryId);
        fetchForumDataOfSportsCategory(countryId);
        fetchForumDataOfScienceCategory(countryId);
        fetchForumDataOfTechnologyCategory(countryId);
    }, [countryId]);

    return (
        <div className="forum-main-page">
            <Menubar />
            <div className="container">
                <div className="forumComponents">
                    <div className="forumTopic">
                        <p className="forumTopic-text"><b>Forum</b></p>
                        <hr className="hrForum-tag" />
                    </div>
                    <div className="selectForm">
                        <Row>
                            <Col span={5}>
                                <p className="select-text"><b>Select the country:</b></p>
                            </Col>
                            <Col span={6}>
                                <SelectCountry />
                            </Col>
                        </Row>
                    </div>
                    <ForumList />
                </div>
            </div>
            <FooterTab/>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        countryId: state.forumData.countryId
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setCountryData,
        fetchForumDataOfBusinessCategory,
        fetchForumDataOfEntertainCategory,
        fetchForumDataOfGeneralCategory,
        fetchForumDataOfHealthCategory,
        fetchForumDataOfSportsCategory,
        fetchForumDataOfScienceCategory,
        fetchForumDataOfTechnologyCategory
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForumPage);
