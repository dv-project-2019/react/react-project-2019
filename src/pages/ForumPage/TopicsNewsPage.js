import React from 'react';
import Menubar from '../../component/Header';
import ForumTopicHeader from '../../component/ForumTopicHeader';
import TopicLists from '../../component/TopicsList';

const TopicsNewsPage = (props) => {
    return (
        <div style={{ backgroundColor: '#F2EDEC', height: '1000px' }}>
            <Menubar />
            <ForumTopicHeader category={props.match.params.category} />
            <div><TopicLists country={props.match.params.country} category={props.match.params.category} /></div>
        </div>
    )
}

export default TopicsNewsPage;