import React from 'react';
import './App.css';
import './TopicsList-style.css'
import HomePage from './pages/HomePage/HomePage';
import { BrowserRouter, Route } from 'react-router-dom';
import ForumPage from './pages/ForumPage/ForumPage';
import TopHeadlinePage from './pages/TopHeadlinePage/TopHeadlinePage';
import TopHeadlineContentPage from './pages/TopHeadlinePage/TopHeadlineContentPage';
import TopicsNewsPage from './pages/ForumPage/TopicsNewsPage';
import ForumContentPage from './pages/ForumPage/forumContent/ForumContentPage';
import AboutUsPage from './pages/AboutUsPage/AboutUsPage';
import HomeContentTopPage  from './pages/HomePage/HomeContentTopPage';
import HomeContentNews from './pages/HomePage/HomeContentNews';
import NewsPage from './pages/NewsPage/NewsPage';
import NewsContentPage from './pages/NewsPage/NewsContentPage';
function App() {
  return (
    <BrowserRouter>
      {/* <Route path="/processLogin" render={() => {
        login()
        return <Redirect to="/users" />
      }} />
      <Route path="/processLogout" render={() => {
        logout()
        return <Redirect to="/login" />
      }} /> */}
      <Route path="/" component={HomePage} exact={true} />
      <Route path="/home" component={HomePage} exact={true} />
      <Route path="/home/t/:country/:index" component={HomeContentTopPage} />
      <Route path="/home/n/:searchText/:index" component={HomeContentNews} />
      <Route path="/forum" component={ForumPage} exact={true} />
      <Route path="/forum/:country/:category" component={TopicsNewsPage} exact={true} />
      <Route path="/forum/:country/:category/:id" component={ForumContentPage} />
      <Route path="/top/:country" component={TopHeadlinePage} exact={true} />
      <Route path="/top/:country/:id" component={TopHeadlineContentPage} />
      {/* <Route path="/news" component={NewsPage} exact={true} /> */}
      <Route path="/news/:keyword" component={NewsPage} exact={true} />
      <Route path="/news/:keyword/:id" component={NewsContentPage} />
      <Route path="/about" component={AboutUsPage} />
      {/* <PrivateRoute path="/users" component={Userpage} exact={true} />
      <PrivateRoute path="/users/:user_id/todos" component={TodoPage} />
      <PrivateRoute path="/users/:user_id/albums" component={AlbumPage} exact={true} />
      <PrivateRoute path="/users/:user_id/albums/:album_id" component={AlbumListPage} /> */}
    </BrowserRouter>
  );
}

export default App;
