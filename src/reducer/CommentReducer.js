import {
    FETCH_COMMENT_DATA_BEGIN,
    FETCH_COMMENT_DATA_SUCCESS,
    FETCH_COMMENT_DATA_ERROR,
    ADD_COMMENT
}
    from "../action/CommentAction";
import Axios from "axios";

const initialState = {
    loading: false,
    error: '',
    comments: []
};

export const commentReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COMMENT_DATA_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_COMMENT_DATA_SUCCESS:
            return {
                ...state,
                loading: false,
                comments: action.commentList,
                error: ''
            }
        case FETCH_COMMENT_DATA_ERROR:
            return {
                ...state,
                loading: false,
                error: ''
            }
        case ADD_COMMENT:
            Axios.post('http://5e44e373e85a4e001492c342.mockapi.io/comment', action.comment)
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
            return {
                ...state,
                comments: [
                    ...state.comments,
                    { ...action.comment },
                ]
            }
        default:
            return state
    }
}