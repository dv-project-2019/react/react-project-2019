import {
    SET_COUNTRY_DATA,
    SET_SELECTED_VALUE,
    FETCH_FORUM_OF_BUSINESS_BEGIN,
    FETCH_FORUM_OF_BUSINESS_SUCCESS,
    FETCH_FORUM_OF_BUSINESS_ERROR,
    SET_CATEGORY_DATA,
    FETCH_FORUM_TOPIC_BEGIN,
    FETCH_FORUM_TOPIC_ERROR,
    FETCH_FORUM_TOPIC_SUCCESS,
    ON_SEARCH_TOPIC_FORUM,
    FETCH_FORUM_OF_ENTERTAIN_BEGIN,
    FETCH_FORUM_OF_ENTERTAIN_SUCCESS,
    FETCH_FORUM_OF_ENTERTAIN_ERROR,
    FETCH_FORUM_OF_GENERAL_BEGIN,
    FETCH_FORUM_OF_GENERAL_SUCCESS,
    FETCH_FORUM_OF_GENERAL_ERROR,
    FETCH_FORUM_OF_HEALTH_BEGIN,
    FETCH_FORUM_OF_HEALTH_SUCCESS,
    FETCH_FORUM_OF_HEALTH_ERROR,
    FETCH_FORUM_OF_SPORTS_BEGIN,
    FETCH_FORUM_OF_SPORTS_SUCCESS,
    FETCH_FORUM_OF_SPORTS_ERROR,
    FETCH_FORUM_OF_SCIENCE_BEGIN,
    FETCH_FORUM_OF_SCIENCE_SUCCESS,
    FETCH_FORUM_OF_SCIENCE_ERROR,
    FETCH_FORUM_OF_TECH_BEGIN,
    FETCH_FORUM_OF_TECH_SUCCESS,
    FETCH_FORUM_OF_TECH_ERROR
} from "../action/ForumAction";

const initialState = {
    countryList: [],
    selectedValue: 'United States',
    countryId: 'us',
    topicCountryId: 'us',
    topicCategory: 'business',
    businessNewsList: [],
    entertainNewsList: [],
    generalNewsList: [],
    healthNewsList: [],
    sportsNewsList: [],
    scienceNewsList: [],
    techNewsList: [],
    loading: false,
    error: '',
    categoryList: [],
    topoicList: [],
    topicSearchText: '',
};

export const forumReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_COUNTRY_DATA:
            return {
                ...state,
                countryList: action.data,
            }
        case SET_SELECTED_VALUE:
            const updateSelectedValue = [...state.countryList];
            var countryName = '';
            updateSelectedValue.map((country) => {
                if (country.id === action.countryValue) {
                    countryName = country.countryName;
                }
            })
            return {
                ...state,
                selectedValue: countryName,
                countryId: action.countryValue
            }
        case FETCH_FORUM_OF_BUSINESS_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_BUSINESS_SUCCESS:
            return {
                ...state,
                loading: false,
                businessNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_BUSINESS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_TOPIC_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_TOPIC_SUCCESS:
            return {
                ...state,
                loading: false,
                topoicList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_TOPIC_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SET_CATEGORY_DATA:
            return {
                ...state,
                categoryList: action.data
            }
        case FETCH_FORUM_OF_ENTERTAIN_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_ENTERTAIN_SUCCESS:
            return {
                ...state,
                loading: false,
                entertainNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_ENTERTAIN_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_OF_GENERAL_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_GENERAL_SUCCESS:
            return {
                ...state,
                loading: false,
                generalNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_GENERAL_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_OF_HEALTH_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_HEALTH_SUCCESS:
            return {
                ...state,
                loading: false,
                healthNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_HEALTH_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_OF_SPORTS_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_SPORTS_SUCCESS:
            return {
                ...state,
                loading: false,
                sportsNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_SPORTS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_OF_SCIENCE_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_SCIENCE_SUCCESS:
            return {
                ...state,
                loading: false,
                scienceNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_SCIENCE_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case FETCH_FORUM_OF_TECH_BEGIN:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case FETCH_FORUM_OF_TECH_SUCCESS:
            return {
                ...state,
                loading: false,
                techNewsList: action.forumData,
                error: ''
            }
        case FETCH_FORUM_OF_TECH_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case ON_SEARCH_TOPIC_FORUM:
            return {
                ...state,
                topicSearchText: action.data
            }
        default:
            return state
    }
}
