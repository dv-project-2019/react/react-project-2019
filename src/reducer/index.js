import { combineReducers } from "redux";
import { forumReducer } from './ForumReducer';
import { homeReducer } from "./HomeReducer";
import { contentForumReducer } from "./ContentForumReducer";
import { commentReducer } from "./CommentReducer";
import { TopHeadlineReducer } from '../reducer/TopHeadlineReducer'
import { NewsPageReducer } from '../reducer/NewsPageReducer'
import { aboutUsReducer } from "./aboutUsReducer";
import { authReducer } from "./AuthReducer";

export const rootReducer = combineReducers({
    forumData: forumReducer,
    forumContentData: contentForumReducer,
    dataComment: commentReducer,
    home: homeReducer,
    TopHeadlineReducer,
    NewsPageReducer,
    aboutUsData: aboutUsReducer,
    auth: authReducer
})
