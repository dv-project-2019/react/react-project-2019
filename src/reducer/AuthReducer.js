import {
    Login, Logout, Fetch_Users_Begin, Fetch_Users_Success, Fetch_Users_Error,
    Change_Username, Change_Password
} from "../action/AuthAction";

const getCurrentUser = (value) => {
    try {
        return JSON.parse(value)
    } catch (e) {
        return null
    }
}
export const initialState = {
    userdata: getCurrentUser(localStorage.getItem("data")),
    users: [],
    loading: false,
    error: '',
    username: '',
    password: '',
    user: {}
};
export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case Fetch_Users_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Users_Success:
            console.log(action.users)
            return {
                ...state,
                users: action.users
            }
        case Fetch_Users_Error:
            return {
                ...state,
                error: action.error
            }
        case Change_Username:
            return {
                ...state,
                username: action.username
            }
        case Change_Password:
            return {
                ...state,
                password: action.password
            }
        case Login:
            if (state.user = state.users.find(users => users.username === state.username && users.password === state.password) === undefined) {
                alert('Username Or Password is not correct')
                localStorage.removeItem('data');
                return {
                    ...state,
                    userdata: null,
                    username: '',
                    password: '',
                }
            } else {
                state.user = state.users.find(users => users.username === state.username && users.password === state.password)
                console.log('state.user', state.user)
                localStorage.setItem("data", JSON.stringify(state.user))
                return {
                    ...state,
                    userdata: state.user,
                    username: '',
                    password: '',
                }
            }
        case Logout:
            localStorage.removeItem('data');
            return {
                ...state,
                userdata: null,
                username: '',
                password: '',
            }
        default:
            return state;
    }
}