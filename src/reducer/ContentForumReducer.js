import {
    Fetch_CONTENT_OF_FORUM_Success,
    Fetch_CONTENT_OF_FORUM_Error,
    Fetch_CONTENT_OF_FORUM_Begin,
    SET_FORUM_CONTENT
}
    from "../action/ContentForumAction";

const initialState = {
    selectedValue: 'United States',
    countryId: 'us',
    loading: false,
    error: '',
    contentList: {},
    contentNews: []
};

export const contentForumReducer = (state = initialState, action) => {
    switch (action.type) {
        case Fetch_CONTENT_OF_FORUM_Begin:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case Fetch_CONTENT_OF_FORUM_Success:
            return {
                ...state,
                loading: false,
                contentNews: action.contentNews,
                error: ''
            }
        case Fetch_CONTENT_OF_FORUM_Error:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        case SET_FORUM_CONTENT:
            return {
                ...state,
                contentList: action.contentList
            }
        default:
            return state
    }
}