import {
    FETCH_HEADLINE_BEGIN, FETCH_HEADLINE_SUCCESS, FETCH_HEADLINE_ERROR,
    FETCH_FILTER_DATA_LIST, SET_TOP_CONTENT
} from '../action/TopHeadlineAction'

const initialState = {
    filter: 'us',
    data: [],
    loading: false,
    error: '',
    content: {}
}

export const TopHeadlineReducer = (state = initialState, action) => {

    switch (action.type) {
        case FETCH_FILTER_DATA_LIST:
            return {
                ...state,
                filter: action.payLoad
            }
        case FETCH_HEADLINE_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_HEADLINE_SUCCESS:
            return {
                ...state,
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_HEADLINE_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case SET_TOP_CONTENT:
            return {
                ...state,
                content: action.payLoad
            }
        default:
            return state
    }
}

