import {
    Fetch_Top_News_Begin, Fetch_Top_News_Success, Fetch_Top_News_Error,
    Fetch_News_Begin, Fetch_News_Success, Fetch_News_Error, Search, SetContent, Change_Country_Text
} from "../action/HomeAction";

export const initialState = {
    topnews: [],
    news: [],
    searchText: 'bitcoin',
    content: {},
    country: 'us',
    loading: false,
    error: ''
};

export const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case Fetch_Top_News_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_Top_News_Success:
            return {
                ...state,
                topnews: action.topnews,
                loading: false
            }
        case Fetch_Top_News_Error:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case Fetch_News_Begin:
            return {
                ...state,
                loading: true
            }
        case Fetch_News_Success:
            return {
                ...state,
                news: action.news,
                loading: false
            }
        case Fetch_News_Error:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case Search:
            return {
                ...state,
                searchText: action.value
            }
        case SetContent:
            return {
                ...state,
                content: action.content
            }
        case Change_Country_Text:
            return {
                ...state,
                country: action.value
            }
        default:
            return state
    }
}