import { SET_FOLLOW_US_DATA } from "../action/aboutUsAction"

const initialState = {
    memberList: []
}

export const aboutUsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_FOLLOW_US_DATA:
            return {
                ...state,
                memberList: action.data
            }
        default:
            return state
    }

}