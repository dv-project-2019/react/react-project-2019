import { SEARCH_NEWS, FETCH_NEWS_BEGIN, FETCH_NEWS_SUCCESS, FETCH_NEWS_ERROR, SET_NEWS_CONTENT } from "../action/NewsPageAction"

const initialState = {
    searchText: 'bitcoin',
    loading: false,
    error: '',
    data: [],
    content: {},
}

export const NewsPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_NEWS:
            return {
                ...state,
                searchText: action.payLoad
            }
        case FETCH_NEWS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_NEWS_SUCCESS:
            return {
                ...state,
                data: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_NEWS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case SET_NEWS_CONTENT:
            return {
                ...state,
                content: action.payLoad
            }
        default:
            return state
    }
}