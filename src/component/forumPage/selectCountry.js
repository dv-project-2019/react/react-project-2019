import React from 'react';
import { Select } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setSelectedValue } from '../../action/ForumAction';

const SelectCountry = (props) => {
    const { Option } = Select;

    const { countryList, selectedValue, setSelectedValue } = props;

    const selectedChange = (value) => {
        setSelectedValue(value);
    }

    return (
        <Select
            defaultValue={selectedValue}
            style={{
                width: 200,
                marginTop: 10,
                marginBottom: 10,
                paddingTop: 10
            }}
            onChange={selectedChange}
        >
            {countryList.map((country, index) => {
                return (
                    <Option key={index} value={country.id}>{country.countryName}</Option>
                )
            })}

        </Select>
    )
}

const mapStateToProps = (state) => {
    return {
        countryList: state.forumData.countryList,
        selectedValue: state.forumData.selectedValue
    }
}

const mapDispatchToProps = (dispatch) => {
    return  bindActionCreators({setSelectedValue}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCountry);