import React, { useEffect } from 'react';
import { List } from 'antd';
import { Row, Col } from 'antd';
import './forumList-style.css';
import { setCategotyData } from '../../action/ForumAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { categoryData } from '../data/categoryData';
import Moment from 'react-moment';

const ForumList = (props) => {
    const {
        setCategotyData,
        categoryList,
        businessNewsList,
        entertainNewsList,
        generalNewsList,
        healthNewsList,
        sportsNewsList,
        scienceNewsList,
        techNewsList,
        countryId,
        loading } = props;

    useEffect(() => {
        setCategotyData(categoryData);
    }, [])

    const getImgToShow = (category) => {

        switch (category.categoryName) {
            case 'Business':
                return "https://image.flaticon.com/icons/png/512/1454/1454827.png";
            case 'Entertainment':
                return "https://therooseveltreview.org/wp-content/uploads/2019/02/icon-28-theater-masks-lg.png";
            case 'General':
                return "https://image.flaticon.com/icons/png/512/1080/1080356.png";
            case 'Health':
                return "https://cdn0.iconfinder.com/data/icons/life-skill-flat-self-improvement/512/healthy_life-512.png";
            case 'Sports':
                return "https://www.pngkey.com/png/full/52-525003_sports-physicals-lake-after-hours.png";
            case 'Science':
                return "https://smsbarbados.files.wordpress.com/2018/09/188802.png";
            case 'Technology':
                return "https://cdn1.iconfinder.com/data/icons/science-technology-bluetone/91/Science__Technology_31-512.png";
            default:
                return "https://image.flaticon.com/icons/png/512/1454/1454827.png"
        }
    }

    const getDataToShow = (category, limit) => {
        switch (category.categoryName) {
            case 'Business':
                return businessNewsList.slice(0, limit);
            case 'Entertainment':
                return entertainNewsList.slice(0, limit);
            case 'General':
                return generalNewsList.slice(0, limit);
            case 'Health':
                return healthNewsList.slice(0, limit);
            case 'Sports':
                return sportsNewsList.slice(0, limit);
            case 'Science':
                return scienceNewsList.slice(0, limit);
            case 'Technology':
                return techNewsList.slice(0, limit);
            default:
                return '';
        }
    }

    const extractsDescription = (description) => {
        if (description === null || description === '') {
            return 'None description';
        } else {
            return description.slice(0, 199);
        }
    }

    const extractsAuthorName = (authorName) => {
        if (authorName === null || authorName === '') {
            return 'None';
        } else {
            return authorName.slice(0, 10);
        }
    }

    return (
        categoryList.map((category, index) => {
            return (
                <div className="forumList">
                    <List
                        key={index}
                        header={
                            <div className="header-topic">
                                <img className="icon" src={getImgToShow(category)} />
                                <p className="header-text"><b>{category.categoryName}</b></p>
                            </div>
                        }
                        footer={
                            <div className="viewMorePost-part">
                                <a href={"/forum/" + countryId + "/" + category.id} className="footer-text" target="_blank">
                                    <b>View more post</b>
                                </a>
                            </div>}
                        bordered
                        itemLayout="vertical"
                        loading={loading}
                        dataSource={getDataToShow(category, 3)}
                        renderItem={(item) => (
                            <List.Item>
                                <Row>
                                    <Col span={19}>
                                        <List.Item.Meta
                                            title={
                                                <Row>
                                                    <Col span={24}>
                                                        <a href={"/forum/" + countryId + "/" + category.id + "/" + getDataToShow(category, 3).indexOf(item)}
                                                            className="news-topic" target="_blank">
                                                            <p>{item.title}</p>
                                                        </a>
                                                    </Col>
                                                </Row>

                                            }
                                            description={
                                                <Row>
                                                    <Col span={20}>
                                                        {extractsDescription(item.description)}...
                                                        </Col>
                                                </Row>
                                            }
                                        />
                                    </Col>

                                    <Col span={5}>
                                        <div className="userAccount">
                                            <Row>
                                                <Col span={18} push={6}>
                                                    <div className="authorData">
                                                        <Row>
                                                            <Col span={18} push={6}>
                                                                <p>By <span><b>{extractsAuthorName(item.author)}</b></span></p>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col span={18} push={6}>
                                                                <p> <Moment format="DD-MM-YYYY HH:mm:ss">{item.publishedAt}</Moment></p>
                                                            </Col>
                                                        </Row>
                                                    </div>

                                                </Col>
                                                <Col span={6} pull={18}>
                                                    <div className="avatarImg-part">
                                                        <img className="avatar-img" src="https://www.shareicon.net/data/512x512/2016/05/26/771204_man_512x512.png" />
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                </Row>
                            </List.Item>
                        )
                        }
                    />
                </div>

            )
        })

    )
}

const mapStateToProps = (state) => {
    return {
        categoryList: state.forumData.categoryList,
        businessNewsList: state.forumData.businessNewsList,
        entertainNewsList: state.forumData.entertainNewsList,
        generalNewsList: state.forumData.generalNewsList,
        healthNewsList: state.forumData.healthNewsList,
        sportsNewsList: state.forumData.sportsNewsList,
        scienceNewsList: state.forumData.scienceNewsList,
        techNewsList: state.forumData.techNewsList,
        countryId: state.forumData.countryId,
        loading: state.forumData.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ setCategotyData }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ForumList);   