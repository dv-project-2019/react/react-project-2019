import React from 'react';
import { Layout } from 'antd';
import '../pages/ForumPage/forumContent/CommentStyle.css';

const { Footer } = Layout;

const FooterTab = () => {
    return (
        <div>
            <Footer className='tabFooter'>Copy right © 2020</Footer>
        </div>
    )
}
export default FooterTab;