export const countryData = [
    {
        id: 'ar',
        countryName: 'Argentina'
    },
    {
        id: 'au',
        countryName: 'Australia'
    },
    {
        id: 'at',
        countryName: 'Austria'
    },
    {
        id: 'be',
        countryName: 'Belgium'
    },
    {
        id: 'br',
        countryName: 'Brazil'
    },
    {
        id: 'bg',
        countryName: 'Bullgaria'
    },
    {
        id: 'ca',
        countryName: 'Canada'
    },
    {
        id: 'cn',
        countryName: 'China'
    },
    {
        id: 'co',
        countryName: 'Colombia'
    },
    {
        id: 'cu',
        countryName: 'Cuba'
    },
    {
        id: 'cz',
        countryName: 'Czech Republic'
    },
    {
        id: 'eg',
        countryName: 'Egypt'
    },
    {
        id: 'fr',
        countryName: 'France'
    },
    {
        id: 'de',
        countryName: 'Germany'
    },
    {
        id: 'gr',
        countryName: 'Greece'
    },
    {
        id: 'hk',
        countryName:'Hong Kong'
    },
    {
        id: 'hu',
        countryName: 'Hungary'
    },
    {
        id: 'in',
        countryName: 'India'
    },
    {
        id: 'id',
        countryName: 'Indonesia'
    },
    {
        id: 'ie',
        countryName: 'Ireland'
    },
    {
        id: 'il',
        countryName: 'Israel'
    },
    {
        id: 'it',
        countryName: 'Italy'
    },
    {
        id: 'jp',
        countryName: 'Japan'
    },
    {
        id: 'lv',
        countryName: 'Latvia'
    },
    {
        id: 'lt',
        countryName: 'Lithuania'
    },
    {
        id: 'my',
        countryName: 'Malaysia'
    },
    {
        id: 'mx',
        countryName: 'Mexico'
    },
    {
        id: 'ma',
        countryName: 'Morocco'
    },
    {
        id: 'nl',
        countryName: 'Netherlands'
    },
    {
        id: 'nz',
        countryName: 'New Zealand'
    },
    {
        id: 'ng',
        countryName: 'Nigeria'
    }, 
    {
        id: 'no',
        countryName: 'Norway'
    },
    {
        id: 'ph',
        countryName: 'Philippines'
    },
    {
        id: 'pl',
        countryName: 'Poland'
    },
    {
        id: 'pt',
        countryName: 'Portugal'
    },
    {
        id: 'ro',
        countryName: 'Romania'
    },
    {
        id: 'ru',
        countryName: 'Russia'
    },
    {
        id: 'sa',
        countryName: 'Saudi Arabia'
    },
    {
        id: 'rs',
        countryName: 'Serbia'
    },
    {
        id: 'sg',
        countryName: 'Singapore'
    },
    {
        id: 'sk',
        countryName: 'Slovakia'
    },
    {
        id: 'si',
        countryName: 'Slovenia'
    },
    {
        id: 'za',
        countryName: 'South Afrika'
    },
    {
        id: 'kr',
        countryName: 'South Korea'
    },
    {
        id: 'se',
        countryName: 'Sweden'
    },
    {
        id: 'ch',
        countryName: 'Switzerland'
    },
    {
        id: 'tw',
        countryName: 'Taiwan'
    },
    {
        id: 'th',
        countryName: 'Thailand'
    },
    {
        id: 'tr',
        countryName: 'Turkey'
    },
    {
        id: 'ae',
        countryName: 'Uae'
    },
    {
        id: 'ua',
        countryName: 'Ukraine'
    },
    {
        id: 'gb',
        countryName: 'United Kingdom'
    },
    {
        id: 'us',
        countryName: 'United States'
    },
    {
        id: 've',
        countryName: 'Venuzuela'
    }
]