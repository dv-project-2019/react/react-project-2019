export const categoryData = [
    {
        id: 'business',
        categoryName: 'Business'
    },
    {
        id: 'entertainment',
        categoryName: 'Entertainment'
    },
    {
        id: 'general',
        categoryName: 'General'
    },
    {
        id: 'health',
        categoryName: 'Health'
    },
    {
        id: 'science',
        categoryName: 'Science'
    },
    {
        id: 'sports',
        categoryName: 'Sports'
    },
    {
        id: 'technology',
        categoryName: 'Technology'
    }
]