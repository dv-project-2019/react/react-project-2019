export const memberData = [
    {
        id: 1,
        firstName: 'Nattapon',
        lastName: 'Promsri',
        studentId: '602110104',
        email: 'nat081584@gmail.com',
        imgUrl: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-1/69999089_2384029811651695_1435072534939697152_o.jpg?_nc_cat=104&_nc_ohc=cj-avWH46nMAX-A2aLW&_nc_ht=scontent-kut2-1.xx&oh=ade2e06de38bc416adf6eeda1880c438&oe=5F0000E8'
    },
    {
        id: 2,
        firstName: 'Preecha',
        lastName: 'Tan',
        studentId: '602110127',
        email: 'Preecha_tan@cmu.ac.th',
        imgUrl: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/70348347_2354387121544236_6097902169229361152_n.jpg?_nc_cat=106&_nc_ohc=4PgzcpgZRBAAX8uJsZx&_nc_ht=scontent-kut2-1.xx&oh=ac1fdaa9939e765b9fff07e14ea60c93&oe=5EBDE398'
    },
    {
        id: 3,
        firstName: 'Ravipon',
        lastName: 'suwanpradhes',
        email: 'Nickzab32514@gmail.com',
        studentId: '602110152',
        imgUrl: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/70492072_2596912253694620_7911085156549525504_n.jpg?_nc_cat=103&_nc_ohc=eW5cHDkg9G4AX8ZDYN1&_nc_ht=scontent-kut2-1.xx&oh=00580f1cab0366c374a358cb5e5c557f&oe=5EFA006F'
    },
    {
        id: 4,
        firstName: 'Poomrapee',
        lastName: 'Kanthapong',
        email: 'poomza209@gmail.com',
        studentId: '602115020',
        imgUrl: 'https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/86169324_2813992768639239_4721526667401494528_n.jpg?_nc_cat=102&_nc_ohc=kH1YOYUM8q8AX-hFpAM&_nc_ht=scontent-kut2-1.xx&oh=bb7b51aaed2ac49662510488e7fdaecf&oe=5F024319'
    },
    {
        id: 5,
        firstName: 'Thipnirun',
        lastName: 'Neamsuntea',
        email: 'Thipnirun.08@gmail.com',
        studentId: '602115011',
        imgUrl: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/p960x960/87259931_1407864162737268_8190767809047822336_o.jpg?_nc_cat=105&_nc_ohc=gR2tRvHSxQ0AX9-V4pJ&_nc_ht=scontent-kut2-2.xx&_nc_tp=6&oh=d320fc50b42906062c637fdaa91e4ef2&oe=5EB93C01'
    }
]