import React, { useState, useEffect } from 'react';
import { Layout, Menu, Icon } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import ModalLogin from './ModalLogin';
import '../pages/ForumPage/forumContent/ListContentForum.css'
import { login, logout, changeUsername, changePassword, fetchUsers } from '../action/AuthAction'
import { fetchComment } from '../action/CommentAction'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import wolfLogo from '../assets/wolf.png';
import '../component/Header.css'

const { Header } = Layout;
const Menubar = (props) => {

    const [modal, setModal] = useState(false);
    const { logout, userdata, fetchComment } = props;
    const toggle = () => {
        setModal(!modal);
    }
    useEffect(() => {
        fetchComment()
    }, [])
    console.log(userdata)
    return (

        <Header>
            <div className="logo" />
            <div className='changBoarder'>
                <Menu
                    justifyContent="center"
                    theme="dark"
                    mode="horizontal"
                    // defaultSelectedKeys={['1']}
                    style={{ lineHeight: '64px', justifyContent: 'end' }}
                >
                    <Menu.Item onClick={() => window.location.href = '/home'} ><img className='sizeLogo' src={wolfLogo} /></Menu.Item>
                    <Menu.Item onClick={() => window.location.href = '/home'}>Home</Menu.Item>
                    <Menu.Item onClick={() => window.location.href = '/top/us'}>Top Headline</Menu.Item>
                    <Menu.Item onClick={() => window.location.href = '/news/bitcoin'}>News</Menu.Item>
                    <Menu.Item onClick={() => window.location.href = '/forum'}>Forum</Menu.Item>
                    <Menu.Item onClick={() => window.location.href = '/about'}>About US</Menu.Item>
                    {userdata === null ?
                        <Menu.Item style={{ float: 'right' }} onClick={toggle}>Login</Menu.Item>
                        :
                        <SubMenu style={{ float: 'right' }} title={<span><Icon type="setting" />{userdata.username}</span>}>
                            <Menu.Item onClick={() => logout()}>Log Out</Menu.Item>
                        </SubMenu>
                    }
                    <ModalLogin visible={modal} hide={toggle} />
                </Menu>
            </div>
        </Header>

    )
}

const mapStateToProps = state => {
    return {
        username: state.auth.username,
        password: state.auth.password,
        userdata: state.auth.userdata

    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ logout, login, changeUsername, changePassword, fetchUsers, fetchComment }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Menubar);
