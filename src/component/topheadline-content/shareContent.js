import React from 'react';
import './shareContent-style.css';
import { Row, Col } from 'antd';

const ShareContent = (props) => {
    return (
        <div className="shareContent-part">
            <Row type="flex" justify="start">
                <Col span={2}>
                    <p>
                        <span>
                            <img className="shareImg" src="https://pngimage.net/wp-content/uploads/2018/06/sharing-icon-png-.png" />
                        </span>
                        <b>Share</b>
                    </p>
                </Col>
                <Col span={1}>
                    <a href={"https://www.facebook.com/sharer.php?u=" + props.url} target="_blank">
                        <img className="fbImg" src="https://4.bp.blogspot.com/-N6pWU2TRKvU/Vzxk2PC6y1I/AAAAAAAAARI/Ml1KTOrt0g8IoR13jJi3wbPPRYID6-vmgCLcB/s1600/square-facebook-512.png" />
                    </a>
                </Col>
                <Col span={1}>
                    <a href={"https://twitter.com/share?url=" + props.url + "&amp;text=" + props.title} target="_blank">
                        <img className="twImg" src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-512.png" />
                    </a>
                </Col>
            </Row>
        </div>
    )
}

export default ShareContent;