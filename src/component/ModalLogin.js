import React, { useEffect } from 'react';
import { Modal } from 'react-bootstrap';
import { Row, Col, Input, Icon, Button } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { login, logout, changeUsername, changePassword, fetchUsers } from '../action/AuthAction'
const ModalLogin = (props) => {

    const { login, changeUsername, changePassword, username, password, fetchUsers } = props;

    useEffect(() => {
        fetchUsers();
    }, [])

    return (
        <Modal show={props.visible} onHide={props.hide} >
            <Modal.Body>
                <div style={{ padding: '0 50px', textAlign: 'center', margin: '0 auto' }}>
                    <Row>
                        <Col span={12} offset={6}>

                            <p>Login</p>
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                                value={username}
                                onChange={(event) => changeUsername(event.target.value)}
                            />
                            <br />
                            <br />
                            <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(event) => changePassword(event.target.value)}
                            />
                            <br />
                            <br />
                            <Button type="primary" onClick={() => {
                                login()
                                props.hide()
                            }} disabled={
                                username === '' || password === '' ?
                                    true
                                    :
                                    false
                            }>
                                Log In
                            </Button>

                        </Col>
                    </Row>

                </div>
            </Modal.Body>
        </Modal>
    )
}
const mapStateToProps = state => {
    return {
        username: state.auth.username,
        password: state.auth.password,

    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ logout, login, changeUsername, changePassword, fetchUsers }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin);