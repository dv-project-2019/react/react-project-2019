import React from 'react'
import { List } from 'antd';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchNewsList } from '../../action/NewsPageAction';
import { Tag } from 'antd';
import Moment from 'react-moment';

const NewsList = (props) => {

    const { dataList, isLoading } = props

    return (
        <List
            loading={isLoading}
            itemLayout="vertical"
            size="small"
            pagination={{
                onChange: page => {
                    console.log(page);
                },
                pageSize: 5,
            }}
            dataSource={
                dataList
            }

            renderItem={item => (
                <List.Item
                    key={item.title}
                    extra={
                        <img
                            height={200}
                            width={300}
                            alt="logo"
                            src={item.urlToImage}
                        />
                    }
                >
                    <List.Item.Meta
                        title={
                            <a href={"/news/" + props.keyword + "/" + dataList.indexOf(item)}
                                className="news-topic" target="_blank" >
                                <p className="news-topic">{item.title}</p>
                            </a>}
                        description={item.description}
                    />
                    <Tag color="green"><Moment format="MM-DD-YYYY">{item.publishedAt}</Moment></Tag>
                    <Tag color="cyan">{item.author}</Tag>
                </List.Item>
            )}
        />
    )
}

const mapStateToProps = (state) => {
    return {
        searchText: state.NewsPageReducer.searchText,
        dataList: state.NewsPageReducer.data,
        isLoading: state.NewsPageReducer.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchNewsList }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);