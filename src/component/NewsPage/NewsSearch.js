import React from 'react'
import { Input } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { searchNews } from '../../action/NewsPageAction'

const { Search } = Input;
const NewsSearch = (props) => {
    
    return (
        <div>
            <div><Search
                placeholder="input search "
                onSearch={(value) => window.location.href = '/news/' + value}
                style={{ width: 200 }}
            /></div>

        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ searchNews }, dispatch)
}

export default connect(mapDispatchToProps)(NewsSearch);

