import React from 'react';
import { Select  } from 'antd';
import { countryData } from '../data/countryData'
import { connect } from 'react-redux';
import { fetchFilterDataList } from '../../action/TopHeadlineAction';
import { bindActionCreators } from 'redux';

const { Option } = Select;
const TopHeadlineFilter = (props) => {
  return (
      <Select defaultValue={props.country} style={{ width: 350 }} onChange={(value) => window.location.href = '/top/' + value}>
        {countryData.map((country) => {
          return (
            <Option value={country.id}>{country.countryName}</Option>
          )
        })}
      </Select>
  )
}

const mapStateToProps = (state) => {
  return {
    countryList: state.forumData.countryList
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ fetchFilterDataList }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TopHeadlineFilter);