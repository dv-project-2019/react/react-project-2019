import React from 'react';
import { List } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchFilterDataList } from '../../action/TopHeadlineAction';
import './topHeadLine.css';
import { Tag } from 'antd';
import Moment from 'react-moment';

const TopHeadlineList = (props) => {

    const { dataList, isLoading } = props;
    console.log(props.country)
    return (

        <List
            loading={isLoading}
            itemLayout="vertical"
            size="small"
            pagination={{
                onChange: page => {
                    console.log(page);
                },
                pageSize: 5,
            }}
            dataSource={dataList}

            renderItem={item => (
                <div className="passClick">
                    <List.Item
                        key={item.title}
                        extra={
                            <img
                                height={200}
                                width={300}
                                alt="logo"
                                src={item.urlToImage}
                            />
                        }
                    >
                        <List.Item.Meta
                            title={
                                <a href={"/top/" + props.country + "/" + dataList.indexOf(item)}
                                    className="news-topic" target="_blank" >
                                    <p className="news-topic">{item.title}</p>
                                </a>}
                            description={item.description}
                        />
                        <Tag color="green"><Moment format="MM-DD-YYYY">{item.publishedAt}</Moment></Tag>
                        <Tag color="cyan" visible={
                            item.author === null || item.author === "" ?
                                false
                                :
                                true
                        }>{item.author}</Tag>
                    </List.Item>
                </div>
            )}
        />
    )
}
const mapStateToProps = (state) => {
    return {
        filter: state.TopHeadlineReducer.filter,
        dataList: state.TopHeadlineReducer.data,
        isLoading: state.TopHeadlineReducer.loading
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchFilterDataList }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(TopHeadlineList);