import React, { useEffect } from 'react';
import { List,  Skeleton } from 'antd';
import { fetchForumTopics } from "../action/ForumAction"
import { bindActionCreators } from 'redux';
import { connect} from 'react-redux';
import Moment from 'react-moment';
import FooterTab from './FooterTab';

const TopicLists = (props) => {

    const { fetchForumTopics, topoicList, topicSearchText, loading } = props
    useEffect(() => {
        fetchForumTopics(props.country, props.category);
    }, [])

    const extractsDescription = (description) => {
        if (description === null || description === '') {
            return '--None description--';
        } else {
            return description.slice(0, 199);
        }
    }

    const extractsAuthorName = (authorName) => {
        if (authorName === null || authorName === '') {
            return 'None';
        } else {
            return authorName.slice(0, 10);
        }
    }

    return (
        <center><div className="topiclist-content">
            {

                <List id="topiclist-eachcontent"
                    className="demo-loadmore-list"
                    loading={loading}
                    pagination={{
                        onChange: page => {
                            console.log(page);
                        },
                        pageSize: 5,

                    }}
                    itemLayout="horizontal"
                    dataSource={
                        topicSearchText === '' ?
                            topoicList
                            :
                            topoicList.filter(m => m.title.match(topicSearchText))
                    }
                    renderItem={item => (
                        <List.Item id="topiclist-detail"
                            actions={[<
                                div>
                                <b>By : {extractsAuthorName(item.author)}</b>
                                <p><Moment format="DD-MM-YYYY HH:mm:ss">{item.publishedAt}</Moment></p>
                            </div>]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active >
                                <List.Item.Meta
                                    title={<a href={"/forum/" + props.country + "/" + props.category + "/" + topoicList.indexOf(item)}
                                        className="news-topic" target="_blank">
                                        <p className="topicListtext">{item.title}</p>
                                    </a>}
                                    description={<p className="topicListtext">{extractsDescription(item.description)}</p>}
                                />
                                <div><img className="avt-img" src="https://www.shareicon.net/data/512x512/2016/05/26/771204_man_512x512.png"></img></div>
                            </Skeleton>
                        </List.Item>
                    )}
                />

            }

        </div>
        <FooterTab/>
        </center >
    );
}

const mapStateToProps = (state) => {
    return {
        topicCountryId: state.forumData.topicCountryId,
        topicCategory: state.forumData.topicCategory,
        topoicList: state.forumData.topoicList,
        topicSearchText: state.forumData.topicSearchText,
        loading: state.forumData.loading
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchForumTopics }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TopicLists);