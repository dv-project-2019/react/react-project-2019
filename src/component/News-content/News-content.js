import React from 'react';
import './newsContent.css';

const NewsContent = (props) => {
    return (
        <div className="topHeadLineContent">
            <div className="newsImg-part">
                <img className="newsImg" alt={props.title} src={props.urlToImage}></img>
            </div>
            <div className="newsCotent">
                <p>{props.description}It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                            <span>
                        <a href={props.url}>Read more</a>
                    </span>
                </p>
            </div>
        </div>
    )
}

export default NewsContent;