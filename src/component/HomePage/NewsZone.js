import React from 'react';
import { PageHeader, Button, Tag,  Card, List } from 'antd';
import { Layout, } from 'antd';
import Meta from 'antd/lib/card/Meta';
import { Input } from 'antd';
import './home.css';
const { Search } = Input;
const { Content } = Layout;

export const NewsZone = (props) => {
    return (
        <PageHeader
            style={{
                border: '1px solid rgb(235, 237, 240)',
            }}
            title={
                <div className="newstopic">
                    <p className="topic-text"><b>{props.head}</b></p>
                </div>
            }
            subTitle={
                <div className="tag-part">
                    <Tag color="magenta">{props.searchText}</Tag>
                </div>

            }
            extra={[
                <>
                    <Search
                        placeholder="input search text"
                        onSearch={value => { props.search(value) }}
                        style={{
                            width: 200,
                            margin: 15
                        }}
                    />
                    <Button type="primary" onClick={() => { window.location.href = '/news/' + props.searchText }}>
                        ReadMore
                </Button>
                </>
            ]}
        >
            <Content>
                <div>
                    <List
                        loading={props.loading}
                        grid={{ gutter: 16, column: 3 }}
                        dataSource={props.data}
                        renderItem={item => (
                            <List.Item>
                                <Card
                                    style={{ padding: '10px', margin: '0 auto', width: 300, height: 330 }}
                                    cover={
                                        <img
                                            style={{ width: 278, height: 145, margin: '0 auto' }}
                                            alt={item.title}
                                            src={item.urlToImage}
                                        />
                                    }>
                                    <Meta
                                        title={<a href={"/home/n/" + props.searchText + "/" + props.data.indexOf(item)}
                                            className="newsList-text">{item.title}</a>}
                                        description={item.description === null || item.description === " " ? item.description : item.description.slice(0, 59)}
                                    />
                                </Card>
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
        </PageHeader>
    )
} 