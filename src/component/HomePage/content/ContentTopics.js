import React from 'react';
import './content.css';
import { Row, Col } from 'antd';
import Moment from 'react-moment';

const ContentTopic = (props) => {
    return (
        <div className="topHeadLineTopic">
            <p className="topHeadLineTopic-text"><b>{props.title}</b></p>
            <hr className="hr-tag" />
            <Row type="flex" justify="center">
                <Col span={5}>
                    <div className="authorName">
                        <p>BY <b>{props.author === null ? <e> None</e> : props.author}</b></p>
                    </div>
                </Col>
                <Col span={5}>
                    <div className="date">
                        <p><b>DATE: </b> <Moment format="DD-MM-YYYY HH:mm:ss">{props.publishedAt}</Moment></p>
                    </div>
                </Col>
            </Row>
        </div>

    )
}

export default ContentTopic;