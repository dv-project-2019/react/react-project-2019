import React from 'react';
import { PageHeader, Button,  Card, List, Select } from 'antd';
import { Layout, } from 'antd';
import Meta from 'antd/lib/card/Meta';
import { countryData } from '../data/countryData';
import './home.css';

const { Content } = Layout;
const { Option } = Select;
const TopHeadlineZone = (props) => {

    return (
        <PageHeader
            style={{
                border: '1px solid rgb(235, 237, 240)',
            }}
            title={
                <div className="topHeadline-topic">
                    <p className="topic-text"><b>{props.head}</b></p>
                </div>
            }
            extra={[
                <>
                    <Select
                        defaultValue="us"
                        style={{
                            width: 200,
                            margin: 15
                        }}
                        onChange={(value) => { props.change(value) }}
                    >
                        {countryData.map((country) => {
                            return (
                                <Option value={country.id}>{country.countryName}</Option>
                            )
                        })}
                    </Select>
                    <Button type="primary" className="readMore-bt" onClick={() => { window.location.href = '/top/' + props.country }}>
                        ReadMore
                    </Button>
                </>
            ]}
        >
            <Content>
                <div>
                    <List
                        loading={props.loading}
                        grid={{ gutter: 16, column: 3 }}
                        dataSource={props.data}
                        renderItem={item => (
                            <List.Item>
                                <Card
                                    style={{ padding: 10, margin: '0px auto', width: 300, height: 330 }}
                                    cover={
                                        <img
                                            style={{ width: 278, height: 145, margin: '0 auto' }}
                                            alt={item.title}
                                            src={item.urlToImage}
                                        />
                                    }>
                                    <Meta
                                        title={
                                            <a href={"/home/t/" + props.country + "/" + props.data.indexOf(item)} className="newsList-text">
                                                <p className="newsList-text">{item.title}</p>
                                            </a>}
                                        description={item.description === null || item.description === " " ? item.description : item.description.slice(0, 59)}
                                    />
                                </Card>
                            </List.Item>
                        )}
                    />
                </div>
            </Content>
        </PageHeader>

    )
}
export default TopHeadlineZone;
