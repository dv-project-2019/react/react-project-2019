import React, { useEffect } from 'react';
import { Carousel } from 'react-bootstrap';
import { fetchTopNews, fetchNews, changeCountry, search } from '../../action/HomeAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
const CarouselZone = (props) => {

    const { fetchTopNews, topnews, country } = props;

    useEffect(() => {
        fetchTopNews(country);
    }, [])

    return (
        <Carousel>
            {topnews.slice(3, 6).map((news) => {
                return (
                    <Carousel.Item>
                        <img
                            className="d-block"
                            width='100%'
                            height='600px'
                            src={news.urlToImage}
                            alt={news.title}
                        />
                        <Carousel.Caption>
                            <div className="textBox">
                                <h3>
                                    <a href={"/home/t/" + country + "/" + topnews.indexOf(news)} className="carouselLinkText">
                                        <b>{news.title}</b>
                                    </a>
                                </h3>
                                <p>{news.description}</p>
                            </div>
                                
                        </Carousel.Caption>
                    </Carousel.Item>
                )
            })}
        </Carousel>
    )
}
const mapStateToProps = state => {
    return {
        topnews: state.home.topnews,
        news: state.home.news,
        searchText: state.home.searchText,
        country: state.home.country
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchTopNews, fetchNews, changeCountry, search }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(CarouselZone);
