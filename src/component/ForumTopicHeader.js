import React from 'react';
import { Input } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {onSearchTopic} from '../action/ForumAction'

const { Search } = Input;

const ForumTopicHeader = (props) => {

    const { onSearchTopic } = props

    return (
        <center><div className="forumtopic-head"><br /><br />
            <h2>{props.category.toUpperCase()}</h2>
            <br /><br />
            <Search
                placeholder="Search news by keyword"
                onChange={e=>onSearchTopic(e.target.value)}
                style={{ width: 250 }}
            />
        </div></center>
    )
}
const mapStateToProps = (state) => {
    return {
        topicCategory: state.forumData.topicCategory
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({onSearchTopic,}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForumTopicHeader);